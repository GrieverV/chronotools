#include "wstring.hh"

void OpenScriptFile(const std::string& filename);
void CloseScriptFile();

void PutAscii(const std::u32string& comment);

void BlockComment(const std::u32string& comment);
void StartBlock(const std::u32string& blocktype, const std::u32string& reason, unsigned intparam);
void StartBlock(const std::u32string& blocktype, const std::u32string& reason);
void PutBase62Label(const unsigned noffs);
void PutBase16Label(const unsigned noffs);
void PutBase10Label(const unsigned noffs);
void PutContent(const std::u32string& s, bool dolf = true);
void EndBlock();

const std::u32string Base62Label(const unsigned noffs);
