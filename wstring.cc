#include <iconv.h>
#include <cerrno>
#include <cstdio>
#include <cstdlib>
#include <cstdarg>
#include <cwchar>

#include "wstring.hh"

#if USE_ICONV
# ifdef WIN32
#  define ICONV_INPUTTYPE const char **
# else
#  define ICONV_INPUTTYPE char **
# endif

namespace
{
    static class midset
    {
        const char* set;
    public:
        midset(): set(NULL)
        {
            const char32_t tmp = 1;
            set = (*(const char *)&tmp == 1) ? "UCS-4LE" : "UCS-4BE";
        }
        inline operator const char*() const
        {
            return set;
        }
    } midset;
}
#else
/* not iconv */
#include <clocale>
#include <cstdlib>
static class EnsureLocale
{
public:
    EnsureLocale() { setlocale(LC_CTYPE, "");
                     setlocale(LC_MESSAGES, "");
                   }
} EnsureLocale;

#endif

wstringOut::wstringOut()
#if USE_ICONV
 : converter(), tester(), charset(midset)
#endif
{
#if USE_ICONV
    converter = iconv_open(charset.c_str(), midset);
    if(converter == (iconv_t)-1) perror("iconv_open");
    tester = iconv_open(charset.c_str(), midset);
    if(tester == (iconv_t)-1) perror("iconv_open");
#endif
}

#if USE_ICONV
wstringOut::wstringOut(const char *setname) : converter(), tester(), charset(midset)
{
    converter = iconv_open(charset.c_str(), midset);
    tester = iconv_open(charset.c_str(), midset);
    SetSet(setname);
}
void wstringOut::SetSet(const char *setname)
{
    iconv_close(converter);
    iconv_close(tester);
    charset = setname;
    converter = iconv_open(setname, midset);
    if(converter == (iconv_t)(-1))
    {
        perror("wstringOut::SetSet::iconv_open");
        exit(1);
    }
    tester = iconv_open(setname, midset);
}
#endif

wstringOut::~wstringOut()
{
#if USE_ICONV
    iconv_close(converter);
    iconv_close(tester);
#endif
}

const std::string wstringOut::putc(char32_t p) const
{
#if USE_ICONV
    std::u32string tmp;
    tmp += p;
    return puts(tmp);
#else
    char* Buf = (char*)alloca(MB_CUR_MAX);
    int n = wctomb(Buf, p);
    if(n < 0) return "";
    return std::string(Buf, Buf+n);
#endif
}

const std::string wstringOut::puts(const std::u32string &s) const
{
#if USE_ICONV
    const char *input = (const char *)(s.data());
    size_t left = s.size() * sizeof(char32_t);
    std::string result;
    while(left > 0)
    {
        char OutBuf[4096], *outptr = OutBuf;
        size_t outsize = sizeof OutBuf;
        size_t retval = iconv(converter,
                              const_cast<ICONV_INPUTTYPE> (&input),
                              &left,
                              &outptr,
                              &outsize);

        std::string tmp(OutBuf, outptr-OutBuf);
        result += tmp;

        if(retval == (size_t)-1)
        {
            if(errno == E2BIG)
            {
                continue;
            }
            if(errno == EILSEQ)
            {
                input += sizeof(char32_t);
                left -= sizeof(char32_t);
                result += '?';
            }
            if(errno == EINVAL)
            {
                /* Got partial byte */
            }
        }
    }
    return result;
#else
    const char32_t *ptr = s.data(), *end = ptr + s.size();
    std::string result;
    while(ptr < end)
    {
        char Buf[4096];
        const char32_t *begin = ptr;
        size_t n = std::wcsrtombs(Buf, &ptr, (sizeof Buf) / sizeof(Buf[0]), NULL);
        bool error = n == (size_t)-1;
        if(!error)
        {
            result.insert(result.end(), Buf, Buf+n);
            continue;
        }
        for(unsigned n2 = ptr - begin; n2 > 0; --n2)
        {
            n = std::wctomb(Buf, *ptr++);
            result.insert(result.end(), Buf, Buf + n);
        }
        if(*ptr) { result += '?'; ++ptr; }
    }
    return result;
#endif
}

bool wstringOut::isok(char32_t p) const
{
#if USE_ICONV
    char OutBuf[256], *outptr = OutBuf;
    const char *tmp = (const char *)(&p);
    size_t outsize = sizeof OutBuf;
    size_t insize = sizeof(p);
    size_t retval = iconv(tester,
                          const_cast<ICONV_INPUTTYPE> (&tmp),
                          &insize,
                          &outptr,
                          &outsize);
    if(retval == (size_t)-1)return false;
    return true;
#else
    char* Buf = (char*)alloca(MB_CUR_MAX);
    return wctomb(Buf, p) >= 0;
#endif
}

wstringIn::wstringIn()
#if USE_ICONV
   : converter(iconv_open(midset, midset)), charset(midset)
#endif
{
#if !USE_ICONV
    std::mbtowc(NULL, NULL, 0);
#endif
}

#if USE_ICONV
wstringIn::wstringIn(const char *setname)
   : converter(iconv_open(midset, midset)), charset(midset)
{
    SetSet(setname);
}
void wstringIn::SetSet(const char *setname)
{
    iconv_close(converter);
    charset = setname;
    converter = iconv_open(midset, setname);
    if(converter == (iconv_t)(-1))
    {
        perror("wstringIn::SetSet::iconv_open");
        exit(1);
    }
}
#endif

wstringIn::~wstringIn()
{
#if USE_ICONV
    iconv_close(converter);
#endif
}

const std::u32string wstringIn::putc(char p) const
{
    std::string tmp;
    tmp += p;
    return puts(tmp);
}

const std::u32string wstringIn::puts(const std::string &s) const
{
#if USE_ICONV
    const char *input = (const char *)(s.data());
    size_t left = s.size();
    std::u32string result;
    while(left > 0)
    {
        char OutBuf[4096], *outptr = OutBuf;
        size_t outsize = sizeof OutBuf;
        size_t retval = iconv(converter,
                              const_cast<ICONV_INPUTTYPE> (&input),
                              &left,
                              &outptr,
                              &outsize);

        //unsigned bytes = (sizeof OutBuf) - outsize;
        unsigned bytes = outptr-OutBuf;
        std::u32string tmp((const char32_t *)(&OutBuf), bytes / (sizeof(char32_t)));
        result += tmp;

        if(retval == (size_t)-1)
        {
            if(errno == E2BIG)
            {
                continue;
            }
            if(errno == EILSEQ)
            {
                input += sizeof(char32_t);
                left -= sizeof(char32_t);
                result += ilseq;
            }
            if(errno == EINVAL)
            {
                /* Got partial byte */
            }
        }
    }
    return result;
#else
    std::u32string result;
    for(unsigned offs=0, left = s.size(); left > 0; )
    {
        char32_t c;
        int n = std::mbtowc(&c, s.data()+offs, left);
        if(n < 0)
        {
            unsigned char byte = s.data()[offs];
            fprintf(stderr, "Ignoring %02X (%c)\n", byte, byte);
            ++offs; --left;
            result += ilseq;
            continue;
        }
        result += c;
        offs += n; left -= n;
    }
    return result;
#endif
}

const std::u32string AscToWstr(const std::string &s)
{
    std::u32string result;
    result.reserve(s.size());
    for(unsigned a=0; a<s.size(); ++a)
        result += AscToWchar(s[a]);
    return result;
}

const std::string WstrToAsc(const std::u32string &s)
{
    std::string result;
    result.reserve(s.size());
    for(unsigned a=0; a<s.size(); ++a)
        result += WcharToAsc(s[a]);
    return result;
}

char WcharToAsc(char32_t c)
{
    // jis ascii
    if(c >= 0xFF10 && c <= 0xFF19) return c - 0xFF10 + '0';
    if(c >= 0xFF21 && c <= 0xFF26) return c - 0xFF21 + 'A';
    if(c >= 0xFF41 && c <= 0xFF46) return c - 0xFF41 + 'a';
    // default
    return static_cast<char> (c);
}

long atoi(const char32_t *p, int base)
{
    long long ret=0;
    bool      sign=false;
    while(*p == '-') { sign=!sign; ++p; }
    for(; *p; ++p)
    {
        char c = WcharToAsc(*p);

        int p = Whex(c);
        if(p == -1)break;
        ret = ret*base + p;
    }
    if(sign) ret=-ret;
    return ret;
}

#if 0
const std::u32string wformat(const char32_t* fmt, ...)
{
    char32_t Buf[4096];
    va_list ap;
    va_start(ap, fmt);
    /* FIXME */
    /*
#ifndef WIN32
    std::vswprintf(Buf, 4096, fmt, ap);
#else
    vswprintf(Buf, fmt, ap);
#endif
    */
    va_end(ap);
    return Buf;
}
#endif

int Whex(char32_t p)
{
    char c = WcharToAsc(p);
    if(c >= '0' && c <= '9') return (c-'0');
    if(c >= 'A' && c <= 'Z') return (c+10-'A');
    if(c >= 'a' && c <= 'z') return (c+10-'a');
    return -1;
}
