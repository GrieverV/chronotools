#include <string>

#include "wstring.hh"
#include "ctcset.hh"

const std::u32string Disp8Char(ctchar k);
const std::u32string Disp12Char(ctchar k);

void LoadDict(unsigned offs, unsigned len);

void DumpDict();

void DumpZStrings(const unsigned offs,
                  const std::u32string& what,
                  unsigned len,
                  bool dolf=true);

void DumpMZStrings(const unsigned offs,
                   const std::u32string& what,
                   unsigned len,
                   bool dolf=true);

void DumpRZStrings(
                  const std::u32string& what,
                  unsigned len,
                  bool dolf,
                  ...);

void Dump8Strings(const unsigned offs,
                  const std::u32string& what,
                  unsigned len);

void DumpC8String(const unsigned offs,
                   const std::u32string& what);

void DumpFStrings(unsigned offs,
                  const std::u32string& what,
                  unsigned len,
                  unsigned maxcount);
